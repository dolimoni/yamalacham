<!-- Latest compiled and minified CSS -->

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">

	<link rel="stylesheet" href="<?php echo base_url('assets/css/sweetalert.css'); ?>">



	<style>
		[ng\:cloak], [ng-cloak], .ng-cloak {
			display: none !important;
		}

	</style>
</head>






<body ng-app="dolimoni" ng-controller="IndexCategoryController">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#"><img style="width: 100px;" src="<?php echo base_url('assets/images/logo.png'); ?>"></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarText">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="<?php echo base_url('admin/category'); ?>">Catégories <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url('admin/product'); ?>">Produits</a>
			</li>
		</ul>
		<span class="navbar-text">
      yamalacham
    </span>
	</div>
</nav>

<div class="container" style="margin-top: 20px;">
	<!-- Content Header (Page header) -->
	<div style="margin-bottom: 30px;" class="content-header">
		<div>


			<p ng-if="editDetails">
				<a ng-click="enableDetails" class="btn btn-warning" data-toggle="collapse" href="#createProduct" role="button"
				   aria-expanded="false" aria-controls="createProduct">
					Ajouter une catégorie
				</a>

			</p>
			<div ng-class="{'show': active}"  class="collapse" id="createProduct">
				<div class="card card-body">
					<div class="row mb-5">
						<div class="col-md-4 col-12">
							<label>Nom anglais</label>
							<input ng-model="category.name" class="form-control"/>
						</div>

						<div class="col-md-4  col-12">
							<label>Nom français</label>
							<input ng-model="category.nameFr" class="form-control"/>
						</div>
						<div class="col-md-4  col-12">
							<label>Nom arabe</label>
							<input ng-model="category.nameAr" class="form-control"/>
						</div>
					</div>

					<div class="row mb-5">
						<div class="col-md-4  col4">
							<label>Photo</label>
							<input type="file" onchange="angular.element(this).scope().uploadImage(this.files)"id="fileInput" name="filedata"/>
						</div>
						<div class="col-md-4  col4">
							<label>Ordre</label>
							<input ng-model="category.catOrder" class="form-control"/>
						</div>
					</div>

					<div class="col-12 text-right">
						<button ng-disabled="disableAdd" ng-click="add()" class="btn btn-success">
							<span  ng-if="editDetails">Terminer</span>
							<span  ng-if="!editDetails">Modifier</span>
						</button>
					</div>
				</div>
			</div>

		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<div class="table-responsive">
		<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
			<tr>
				<th>Nom anglais</th>
				<th>Nom français</th>
				<th>Nom arabe</th>
				<th>Photo</th>
				<th>Action</th>
			</tr>
			</thead>
			<tbody>
			<tr ng-cloak ng-repeat="category in categories">
				<td>{{category.name}}</td>
				<td>{{category.nameFr}}</td>
				<td>{{category.nameAr}}</td>
				<td><img style="width: 75px;" src="{{category.picturePath}}"></td>
				<th>
					<button scroll-on-click href="#createProduct" ng-click="edit(category)" class="btn btn-info">Modifier</button>
					<button ng-click="delete(category)" class="btn btn-danger">Supprimer</button>
				</th>
			</tr>

			</tbody>
			<tfoot>
			<tr>
				<th>Nom anglais</th>
				<th>Nom français</th>
				<th>Nom arabe</th>
				<th>Photo</th>
				<th>
					Actions
				</th>
			</tr>
			</tfoot>
		</table>
	</div>
</div>

</body>




<!--init angularjs data-->
<script>
	js_categories =<?php echo json_encode($categories);?>;
</script>


<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js'); ?>"></script>
<script>
	var PHP_BASE_URL="<?php echo base_url(); ?>";
</script>

<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/app/dolimoni.js'); ?>"></script>
<script src="<?php echo base_url('assets/app/controllers/admin/indexCategoryController.js'); ?>"></script>
<script src="<?php echo base_url('assets/app/services/categoryService.js'); ?>"></script>
<script src="<?php echo base_url('assets/app/directive/scrollOnClick.js'); ?>"></script>

<script>
	$(document).ready(function() {
		$('#example').DataTable();
	} );
</script>
