<?php

class Model_category extends MY_Model {

	private $response = array(
		'status'=>'success',
		'msg'=>'',
	);
	/**
	 * @return array
	 */
	public function getResponse()
	{
		return $this->response;
	}
	public function __construct()
	{
		$this->table = 'category';
	}

	public function add($category){

		if (isset($category['catid'])){
			$id = $category['catid'];
			unset($category['catid']);
			$this->db->where('catid',$id);
			$this->db->update('category',$category);
		}else{
			$this->db->insert('category',$category);

			$catid = $this->db->insert_id();
		}
		$this->model_util->addVersion();
	}


	public function delete($id){

		$this->db->where('catid',$id);
		$this->db->delete('category');
		$this->model_util->addVersion();
	}




	public function getAllWithProducts(){
		$this->db->select('*,catid as id');
		$this->db->order_by('catOrder','asc');
		$categories = $this->db->get($this->table)->result_array();
		foreach ($categories as $key => $category){
			$products = $this->model_product->getByCategory($category['catid']);
			$categories[$key]['products'] = $products;
			$categories[$key]['id'] = intval($category['id']);
		}
		return $categories;
	}



}


