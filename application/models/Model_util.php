<?php

class Model_util extends MY_Model
{


	public function readExcel()
	{

		$this->load->library('excel');

		if (isset($_FILES["file"]["name"])) {
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach ($object->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for ($row = 2; $row <= $highestRow; $row++) {
					$cin = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$prenom = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$nom = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$workTime = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$data[] = array($cin, $prenom, $nom, $workTime);
				}
			}
		}
		return $data;
	}

	public function uploadImage()
	{


		$allow = array("jpg", "jpeg", "gif", "png");

		$todir = FCPATH . 'assets/images/';

		file_put_contents('logs_api.txt', json_encode($_FILES) . PHP_EOL, FILE_APPEND | LOCK_EX);

		if (!!$_FILES['file']['tmp_name']) // is the file uploaded yet?
		{
			file_put_contents('logs_api.txt', "A" . PHP_EOL, FILE_APPEND | LOCK_EX);
			$info = explode('.', strtolower($_FILES['file']['name'])); // whats the extension of the file

			if (in_array(end($info), $allow) or true) // is this file allowed
			{
				file_put_contents('logs_api.txt', "B" . PHP_EOL, FILE_APPEND | LOCK_EX);
				if (move_uploaded_file($_FILES['file']['tmp_name'], $todir . basename($_FILES['file']['name']))) {
					file_put_contents('logs_api.txt', $_FILES['file']['name'] . PHP_EOL, FILE_APPEND | LOCK_EX);
					file_put_contents('logs_api.txt', basename($_FILES['file']['name']) . PHP_EOL, FILE_APPEND | LOCK_EX);
					return basename($_FILES['file']['name']);
				}
			} else {
				var_dump("D");
				die();
				return false;
			}
		}
	}

	public function addVersion()
	{
		$this->db->insert('version', array('value' => 'nothing'));
	}

	public function getCurrentVersion()
	{
		$this->db->select_max('version');
		$this->db->from('version');
		$query = $this->db->get()->row_array();

		return $query['version'];
	}


}

