<?php

class Model_product extends MY_Model {

	private $response = array(
		'status'=>'success',
		'msg'=>'',
	);
	/**
	 * @return array
	 */
	public function getResponse()
	{
		return $this->response;
	}
	public function __construct()
	{
		$this->table = 'product';
	}

	public function add($product){

		if (isset($product['prid'])){
			$id = $product['prid'];
			unset($product['prid']);
			unset($product['categoryName']);
			$this->db->where('prid',$id);
			$this->db->update('product',$product);
		}else{
			$this->db->insert('product',$product);

			$companyId = $this->db->insert_id();
		}

		$this->model_util->addVersion();
	}


	public function delete($id){

		$this->db->where('prid',$id);
		$this->db->delete('product');

		$this->model_util->addVersion();
	}


	public function getByCategory($catid){
		$this->db->select('*,descEn as desc,prid as id');
		$this->db->where('catid',$catid);
		$products = $this->db->get('product')->result_array();

		foreach ($products as $key => $product){
			$products[$key]['id']=intval($product['id']);
		}

		return $products;
	}

	public function getAll()
	{
		$this->db->select('p.*,c.nameFr as categoryName');
		$this->db->from('product p');
		$this->db->join('category c','p.catid = c.catid','left');
		return $this->db->get()->result_array();
	}

}


