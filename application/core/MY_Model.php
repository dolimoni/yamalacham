<?php

class MY_Model extends CI_Model {

	protected $table ;

	public function __construct()
	{

	}

	public function getAll(){
		return $this->db->get($this->table)->result_array();
	}

	public function getBy($key,$value,$singleRow = false){
		$this->db->where($key,$value);
		if($singleRow){
			return $this->db->get($this->table)->row_array();
		}else{
			return $this->db->get($this->table)->result_array();
		}
	}

}
