<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {


	public function index()
	{
		$data['categories'] = $this->model_category->getAll();
		$data['products'] = $this->model_product->getAll();
		$this->load->view('admin/products/index_view',$data);
	}
}
