<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {


	public function index()
	{
		$data['categories'] = $this->model_category->getAll();
		$this->load->view('admin/categories/index_view',$data);
	}
}
