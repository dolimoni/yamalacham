<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *URL
	 * Maps to the following
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function add()
	{
		try{
		$category = $this->input->post('category');
		$this->model_category->add($category);
		$response=$this->model_category->getResponse();

			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		} catch (Exception $e) {
			$response['status']="error";
			$response['msg']=$e->getMessage();
			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		}
	}



	public function delete()
	{
		try{
		$catid = $this->input->post('catid');
		$this->model_category->delete($catid);
		$response=$this->model_category->getResponse();

			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		} catch (Exception $e) {
			$response['status']="error";
			$response['msg']=$e->getMessage();
			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		}
	}

	public function deleteAffectation()
	{
		try{
		$affid = $this->input->post('affid');
		$this->model_company->deleteAffectation($affid);
		$response=$this->model_company->getResponse();

			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		} catch (Exception $e) {
			$response['status']="error";
			$response['msg']=$e->getMessage();
			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		}
	}
}
