<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Utils extends CI_Controller
{

	/**
	 *     * Index Page for this controller.
	 *         *URL
	 *             * Maps to the following
	 *                 *        http://example.com/index.php/welcome
	 *                     *    - or -
	 *                         *        http://example.com/index.php/welcome/index
	 *                             *    - or -
	 *                                 * Since this controller is set as the default controller in
	 *                                     * config/routes.php, it's displayed at http://example.com/
	 *                                         *
	 *                                             * So any other public methods not prefixed with an underscore will
	 *                                                 * map to /index.php/welcome/<method_name>
	 *                                                     * @see https://codeigniter.com/user_guide/general/urls.html
	 *                                                         */
	public function uploadImage()
	{
		try {


			$image = $this->model_util->uploadImage();
			$status = "success";
			if ($image) {
				$image = "assets/images/" . $image;
			} else {
				$status = "warning";
			}
			$response = array(
				'image' => $image,
				'status' => $status
			);
			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		} catch (Exception $e) {
			$response['status'] = "error";
			$response['msg'] = $e->getMessage();
			$this->output
				->set_content_type("application/json")
				->set_output(json_encode($response));
		}
	}

}

