
dolimoni.service('categoryService',function($http,BASE_URL){

	this.add=function (category){
		var request='admin/api/category/add/';

		var data = $.param({
			category: category
		});

		return $http({
			method: 'POST',
			data:data,
			url: BASE_URL+request,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

		});
	};


	this.delete=function (catid){
		var request='admin/api/category/delete/';

		var data = $.param({
			catid: catid
		});

		return $http({
			method: 'POST',
			data:data,
			url: BASE_URL+request,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

		});
	};


	this.uploadImage=function (image){
		var request='admin/api/utils/uploadImage/';
		var fd = new FormData();
		//Take the first selected file
		fd.append("file", image);
		return $http({
			method: 'POST',
			data:fd,
			url: BASE_URL+request,
			headers: {'Content-Type': undefined },
		});
	};
});
