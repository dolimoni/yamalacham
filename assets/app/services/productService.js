
dolimoni.service('productService',function($http,BASE_URL){

	this.add=function (product){
		var request='admin/api/product/add/';

		var data = $.param({
			product: product
		});

		return $http({
			method: 'POST',
			data:data,
			url: BASE_URL+request,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

		});
	};


	this.delete=function (prid){
		var request='admin/api/product/delete/';

		var data = $.param({
			prid: prid
		});

		return $http({
			method: 'POST',
			data:data,
			url: BASE_URL+request,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

		});
	};

	this.uploadImage=function (image){
		var request='admin/api/utils/uploadImage/';
		var fd = new FormData();
		//Take the first selected file
		fd.append("file", image);
		return $http({
			method: 'POST',
			data:fd,
			url: BASE_URL+request,
			headers: {'Content-Type': undefined },
		});
	};
});
