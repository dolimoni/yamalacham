dolimoni.controller('IndexCategoryController',['$scope','BASE_URL','categoryService',function($scope,BASE_URL,categoryService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.categories=js_categories;

    vm.editDetails = true;

	vm.category= {
		name:'',
		nameFr:'',
		nameAr:'',
	};

	vm.edit = function(category){
		vm.category = category;
		vm.active=true;
	};

	vm.uploadImage=function(image)
	{

		vm.editDetails = false;
		categoryService.uploadImage(image[0]).then(function success(data) {
			if(data.data.status=="success"){
				vm.editDetails = true;
				vm.category.picturePath=BASE_URL+'/'+data.data.image;
			}else{
				alert(data.data.msg);
			}

		});
	}

	vm.add = function () {

		var cleanObject = JSON.parse(angular.toJson(vm.category));
		vm.disableAdd = true;
		categoryService.add(cleanObject).then(function success(data) {

			vm.disableAdd = false;
			if(data.data.status=="success"){
				location.reload();
			}else{
				alert(data.data.msg);
			}

		});
	};


	vm.delete = function (category){
		var text = "Vous voulez vraiment supprimer cet categorie ?";
		//swal({title: "Erreur", text: text, type: "warning", showConfirmButton: true});


		swal({
			title: "Attention",
			text: text,
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonText: 'Confirmer',
			cancelButtonText: 'Annuler',
			dangerMode: false,
		}, function () {
			categoryService.delete(category.catid).then(function success(data) {
				location.reload();
			});
		});
	}


}]);
