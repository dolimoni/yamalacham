dolimoni.controller('IndexProductController',['$scope','BASE_URL','productService',function($scope,BASE_URL,productService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.products=js_products;
    vm.categories=js_categories;

    vm.editDetails = true;

	vm.product= {
		name:'',
		nameFr:'',
		nameAr:'',
	};

	vm.edit = function(product){
		vm.product = product;
		vm.editDetails = false;
		vm.active=true;
	};

	vm.add = function () {

		var cleanObject = JSON.parse(angular.toJson(vm.product));
		vm.disableAdd = true;
		productService.add(cleanObject).then(function success(data) {

			vm.disableAdd = false;
			if(data.data.status=="success"){
				location.reload();
			}else{
				alert(data.data.msg);
			}

		});
	};


	vm.delete = function (product){
		var text = "Vous voulez vraiment supprimer cet categorie ?";
		//swal({title: "Erreur", text: text, type: "warning", showConfirmButton: true});

		swal({
			title: "Attention",
			text: text,
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonText: 'Confirmer',
			cancelButtonText: 'Annuler',
			dangerMode: false,
		}, function () {
			productService.delete(product.prid).then(function success(data) {
				location.reload();
			});
		});
	}

	vm.uploadImage=function(image)
	{
		vm.editDetails = false;
		productService.uploadImage(image[0]).then(function success(data) {
			vm.editDetails = true;
			if(data.data.status=="success"){
				vm.product.picturePath=BASE_URL+'/'+data.data.image;
			}else{
				alert(data.data.msg);
			}

		});
	}



}]);
